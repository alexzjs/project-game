package network

import (
	"errors"
	"sync"
)

type Conn interface {
	WriteMessage(messageType uint32, data []byte) error
	Addr() string
	Close() error
}

type Session struct {
	locker         sync.RWMutex
	ID             string
	properties     map[string]interface{}
	conn           Conn
	postProcessing []func(sess *Session)
}

func (ts *Session) AddPostProcessing(fn func(sess *Session)) {
	ts.locker.Lock()
	defer ts.locker.Unlock()

	ts.postProcessing = append(ts.postProcessing, fn)
}

func (ts *Session) PostProcessing() {
	ts.locker.Lock()
	defer ts.locker.Unlock()

	for _, fn := range ts.postProcessing {
		fn(ts)
	}
}

func (ts *Session) RemoteAddr() string {
	ts.locker.RLock()
	defer ts.locker.RUnlock()

	return ts.conn.Addr()
}

func (ts *Session) Close() error {
	ts.locker.Lock()
	defer ts.locker.Unlock()

	return ts.conn.Close()
}

func (ts *Session) SetProperty(key string, value interface{}) {
	ts.locker.Lock()
	ts.properties[key] = value
	ts.locker.Unlock()
}

func (ts *Session) GetProperty(key string) (interface{}, bool) {
	ts.locker.RLock()
	defer ts.locker.RUnlock()

	p, ok := ts.properties[key]
	return p, ok
}

func (ts *Session) RemoveProperty(key string) {
	ts.locker.RLock()
	defer ts.locker.RUnlock()
	delete(ts.properties, key)
}

var ErrSessionDisconnected = errors.New("this session is broken")

func (ts *Session) Emit(messageType uint32, message []byte) error {
	if ts.conn == nil {
		return ErrSessionDisconnected
	}
	return ts.conn.WriteMessage(messageType, message)
}
