package MessageProtocol

import (
	"google.golang.org/protobuf/proto"
)

type ProtobufProtocol struct{}

func NewProtobufProtocol() *ProtobufProtocol {
	return &ProtobufProtocol{}
}

func (ts *ProtobufProtocol) Marshal(message interface{}) ([]byte, error) {
	return proto.Marshal(message.(proto.Message))
}

func (ts *ProtobufProtocol) Unmarshal(data []byte, messageType interface{}) error {
	return proto.Unmarshal(data, messageType.(proto.Message))
}
