package logger

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"log"
	"os"
	"runtime"
	"runtime/debug"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

var defaultlog *logBean = getdefaultLogger()
var skip int = 4

type logger struct {
	lb *logBean
}

func (ts *logger) SetConsole(isConsole bool) {
	ts.lb.setConsole(isConsole)
}

func (ts *logger) SetLevel(_level LEVEL) {
	ts.lb.setLevel(_level)
}

func (ts *logger) SetFormat(logFormat string) {
	ts.lb.setFormat(logFormat)
}

func (ts *logger) SetRollingFile(fileDir, fileName string, maxNumber int32, maxSize int64, _unit UNIT) {
	ts.lb.setRollingFile(fileDir, fileName, maxNumber, maxSize, _unit)
}

func (ts *logger) SetRollingDaily(fileDir, fileName string) {
	ts.lb.setRollingDaily(fileDir, fileName)
}

func (ts *logger) Debug(v ...interface{}) {
	ts.lb.debug(v...)
}
func (ts *logger) Info(v ...interface{}) {
	ts.lb.info(v...)
}
func (ts *logger) Warn(v ...interface{}) {
	ts.lb.warn(v...)
}
func (ts *logger) Error(v ...interface{}) {
	ts.lb.error(v...)
}
func (ts *logger) Fatal(v ...interface{}) {
	ts.lb.fatal(v...)
}

func (ts *logger) SetLevelFile(level LEVEL, dir, fileName string) {
	ts.lb.setLevelFile(level, dir, fileName)
}

type logBean struct {
	mu              *sync.Mutex
	logLevel        LEVEL
	maxFileSize     int64
	maxFileCount    int32
	consoleAppender bool
	rolltype        ROLLTYPE
	format          string
	id              string
	d, i, w, e, f   string //id
}

type fileBeanFactory struct {
	fbs map[string]*fileBean
	mu  *sync.RWMutex
}

var fbf = &fileBeanFactory{fbs: make(map[string]*fileBean, 0), mu: new(sync.RWMutex)}

func (ts *fileBeanFactory) add(dir, filename string, _suffix int, maxsize int64, maxfileCount int32) {
	ts.mu.Lock()
	defer ts.mu.Unlock()
	id := md5str(fmt.Sprint(dir, filename))
	if _, ok := ts.fbs[id]; !ok {
		ts.fbs[id] = newFileBean(dir, filename, _suffix, maxsize, maxfileCount)
	}
}

func (ts *fileBeanFactory) get(id string) *fileBean {
	ts.mu.RLock()
	defer ts.mu.RUnlock()
	return ts.fbs[id]
}

type fileBean struct {
	id           string
	dir          string
	filename     string
	_suffix      int
	_date        *time.Time
	mu           *sync.RWMutex
	logfile      *os.File
	lg           *log.Logger
	filesize     int64
	maxFileSize  int64
	maxFileCount int32
}

func GetLogger() (l *logger) {
	l = new(logger)
	l.lb = getdefaultLogger()
	return
}

func getdefaultLogger() (lb *logBean) {
	lb = &logBean{}
	lb.mu = new(sync.Mutex)
	lb.setConsole(true)
	return
}

func (ts *logBean) setConsole(isConsole bool) {
	ts.consoleAppender = isConsole
}

func (ts *logBean) setLevelFile(level LEVEL, dir, fileName string) {
	key := md5str(fmt.Sprint(dir, fileName))
	switch level {
	case DEBUG:
		ts.d = key
	case INFO:
		ts.i = key
	case WARN:
		ts.w = key
	case ERROR:
		ts.e = key
	case FATAL:
		ts.f = key
	default:
		return
	}
	var _suffix = 0
	if ts.maxFileCount < 1<<31-1 {
		for i := 1; i < int(ts.maxFileCount); i++ {
			if isExist(dir + "/" + fileName + "." + strconv.Itoa(i)) {
				_suffix = i
			} else {
				break
			}
		}
	}
	fbf.add(dir, fileName, _suffix, ts.maxFileSize, ts.maxFileCount)
}

func (ts *logBean) setLevel(_level LEVEL) {
	ts.logLevel = _level
}

func (ts *logBean) setFormat(logFormat string) {
	ts.format = logFormat
}

func (ts *logBean) setRollingFile(fileDir, fileName string, maxNumber int32, maxSize int64, _unit UNIT) {
	ts.mu.Lock()
	defer ts.mu.Unlock()
	if maxNumber > 0 {
		ts.maxFileCount = maxNumber
	} else {
		ts.maxFileCount = 1<<31 - 1
	}
	ts.maxFileSize = maxSize * int64(_unit)
	ts.rolltype = ROLLFILE
	mkdirlog(fileDir)
	var _suffix = 0
	for i := 1; i < int(maxNumber); i++ {
		if isExist(fileDir + "/" + fileName + "." + strconv.Itoa(i)) {
			_suffix = i
		} else {
			break
		}
	}
	ts.id = md5str(fmt.Sprint(fileDir, fileName))
	fbf.add(fileDir, fileName, _suffix, ts.maxFileSize, ts.maxFileCount)
}

func (ts *logBean) setRollingDaily(fileDir, fileName string) {
	ts.rolltype = DAILY
	mkdirlog(fileDir)
	ts.id = md5str(fmt.Sprint(fileDir, fileName))
	fbf.add(fileDir, fileName, 0, 0, 0)
}

func (ts *logBean) console(v ...interface{}) {
	s := fmt.Sprint(v...)
	if ts.consoleAppender {
		_, file, line, _ := runtime.Caller(skip)
		short := file
		for i := len(file) - 1; i > 0; i-- {
			if file[i] == '/' {
				short = file[i+1:]
				break
			}
		}
		file = short
		if ts.format == "" {
			log.Println(file, strconv.Itoa(line), s)
		} else {
			vs := make([]interface{}, 0)
			vs = append(vs, file)
			vs = append(vs, strconv.Itoa(line))
			for _, vv := range v {
				vs = append(vs, vv)
			}
			log.Printf(fmt.Sprint("%s %s ", ts.format, "\n"), vs...)
		}
	}
}

func (ts *logBean) log(level string, v ...interface{}) {
	defer catchError()
	s := fmt.Sprint(v...)
	length := len([]byte(s))
	var lg *fileBean = fbf.get(ts.id)
	var _level = ALL
	switch level {
	case "debug":
		if ts.d != "" {
			lg = fbf.get(ts.d)
		}
		_level = DEBUG
	case "info":
		if ts.i != "" {
			lg = fbf.get(ts.i)
		}
		_level = INFO
	case "warn":
		if ts.w != "" {
			lg = fbf.get(ts.w)
		}
		_level = WARN
	case "error":
		if ts.e != "" {
			lg = fbf.get(ts.e)
		}
		_level = ERROR
	case "fatal":
		if ts.f != "" {
			lg = fbf.get(ts.f)
		}
		_level = FATAL
	}
	if lg != nil {
		ts.fileCheck(lg)
		lg.addsize(int64(length))
		if ts.logLevel <= _level {
			if lg != nil {
				if ts.format == "" {
					lg.write(level, s)
				} else {
					lg.writef(ts.format, v...)
				}
			}
			ts.console(v...)
		}
	} else {
		ts.console(v...)
	}
}

func (ts *logBean) debug(v ...interface{}) {
	ts.log("debug", v...)
}
func (ts *logBean) info(v ...interface{}) {
	ts.log("info", v...)
}
func (ts *logBean) warn(v ...interface{}) {
	ts.log("warn", v...)
}
func (ts *logBean) error(v ...interface{}) {
	ts.log("error", v...)
}
func (ts *logBean) fatal(v ...interface{}) {
	ts.log("fatal", v...)
}

func (ts *logBean) fileCheck(fb *fileBean) {
	defer catchError()
	if ts.isMustRename(fb) {
		ts.mu.Lock()
		defer ts.mu.Unlock()
		if ts.isMustRename(fb) {
			fb.rename(ts.rolltype)
		}
	}
}

//--------------------------------------------------------------------------------

func (ts *logBean) isMustRename(fb *fileBean) bool {
	switch ts.rolltype {
	case DAILY:
		t, _ := time.Parse(_DATEFORMAT, time.Now().Format(_DATEFORMAT))
		if t.After(*fb._date) {
			return true
		}
	case ROLLFILE:
		return fb.isOverSize()
	}
	return false
}

func (ts *fileBean) nextSuffix() int {
	return int(ts._suffix%int(ts.maxFileCount) + 1)
}

func newFileBean(fileDir, fileName string, _suffix int, maxSize int64, maxfileCount int32) (fb *fileBean) {
	t, _ := time.Parse(_DATEFORMAT, time.Now().Format(_DATEFORMAT))
	fb = &fileBean{dir: fileDir, filename: fileName, _date: &t, mu: new(sync.RWMutex)}
	fb.logfile, _ = os.OpenFile(fileDir+"/"+fileName, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0666)
	fb.lg = log.New(fb.logfile, "", log.Ldate|log.Ltime|log.Lshortfile)
	fb._suffix = _suffix
	fb.maxFileSize = maxSize
	fb.maxFileCount = maxfileCount
	fb.filesize = fileSize(fileDir + "/" + fileName)
	fb._date = &t
	return
}

func (ts *fileBean) rename(rolltype ROLLTYPE) {
	ts.mu.Lock()
	defer ts.mu.Unlock()
	ts.close()
	nextfilename := ""
	switch rolltype {
	case DAILY:
		nextfilename = fmt.Sprint(ts.dir, "/", ts.filename, ".", ts._date.Format(_DATEFORMAT))
	case ROLLFILE:
		nextfilename = fmt.Sprint(ts.dir, "/", ts.filename, ".", ts.nextSuffix())
		ts._suffix = ts.nextSuffix()
	}
	if isExist(nextfilename) {
		os.Remove(nextfilename)
	}
	os.Rename(ts.dir+"/"+ts.filename, nextfilename)
	t, _ := time.Parse(_DATEFORMAT, time.Now().Format(_DATEFORMAT))
	ts._date = &t
	ts.logfile, _ = os.OpenFile(ts.dir+"/"+ts.filename, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0666)
	ts.lg = log.New(ts.logfile, "", log.Ldate|log.Ltime|log.Lshortfile)
	ts.filesize = fileSize(ts.dir + "/" + ts.filename)
}

func (ts *fileBean) addsize(size int64) {
	ts.mu.Lock()
	defer ts.mu.Unlock()

	atomic.AddInt64(&ts.filesize, size)
}

func (ts *fileBean) write(level string, v ...interface{}) {
	ts.mu.RLock()
	defer ts.mu.RUnlock()
	s := fmt.Sprint(v...)
	ts.lg.Output(skip+1, fmt.Sprintln(level, s))
}

func (ts *fileBean) writef(format string, v ...interface{}) {
	ts.mu.RLock()
	defer ts.mu.RUnlock()
	ts.lg.Output(skip+1, fmt.Sprintf(format, v...))
}

func (ts *fileBean) isOverSize() bool {
	ts.mu.RLock()
	defer ts.mu.RUnlock()
	return ts.filesize >= ts.maxFileSize
}

func (ts *fileBean) close() {
	ts.logfile.Close()
}

//-----------------------------------------------------------------------------------------------

func mkdirlog(dir string) (e error) {
	_, er := os.Stat(dir)
	b := er == nil || os.IsExist(er)
	if !b {
		if err := os.MkdirAll(dir, 0666); err != nil {
			if os.IsPermission(err) {
				e = err
			}
		}
	}
	return
}

func fileSize(file string) int64 {
	f, e := os.Stat(file)
	if e != nil {
		fmt.Println(e.Error())
		return 0
	}
	return f.Size()
}

func isExist(path string) bool {
	_, err := os.Stat(path)
	return err == nil || os.IsExist(err)
}

func md5str(s string) string {
	m := md5.New()
	m.Write([]byte(s))
	return hex.EncodeToString(m.Sum(nil))
}

func catchError() {
	if err := recover(); err != nil {
		fmt.Println(string(debug.Stack()))
	}
}
