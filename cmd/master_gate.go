package main

import (
	"flag"
	"fmt"
	"gitee.com/alexzjs/project-game"
	"gitee.com/alexzjs/project-game/core/ecs"
	"gitee.com/alexzjs/project-game/internal/gate"
	"gitee.com/alexzjs/project-game/internal/gate/logic"
	"gitee.com/alexzjs/project-game/utils/logger"
	"net/http"
	_ "net/http/pprof"
)

var Server *pg.Server

func main() {
	var nodeConfName string
	flag.StringVar(&nodeConfName, "node", "", "node info")
	flag.Parse()

	/*  pprof */
	go func() {
		logger.Info(http.ListenAndServe("localhost:7077", nil))
	}()

	Server = pg.DefaultServer()

	//重选节点
	if nodeConfName != "" {
		Server.OverrideNodeDefine(nodeConfName)
		logger.Info(fmt.Sprintf("Override node info:[ %s ]", nodeConfName))
	}

	g := &gate.WSGateComponent{}

	//非默认网关不必要这样初始化，在组件内初始化即可，此处是为了对默认网关注入
	g.AddNetAPI(logic.NewTestApi())

	Server.AddComponentGroup("gate", []ecs.IComponent{g})
	Server.AddComponentGroup("room", []ecs.IComponent{&logic.RoomManagerComponent{}})
	Server.Serve()
}
