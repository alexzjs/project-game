package pg

import (
	"gitee.com/alexzjs/project-game/core/ecs"
	"gitee.com/alexzjs/project-game/launcher"
	"runtime"
	"time"
)

var defaultNode *Server

type Server = launcher.LauncherComponent

//新建一个服务节点
func NewServerNode() *Server {
	//构造运行时
	r := ecs.NewRuntime(ecs.Config{ThreadPoolSize: runtime.NumCPU()})
	r.UpdateFrameByInterval(time.Millisecond * 100)

	//构造启动器
	l := &launcher.LauncherComponent{}
	r.Root().AddComponent(l)
	return l
}

//获取默认节点
func DefaultServer() *Server {
	if defaultNode != nil {
		return defaultNode
	}
	defaultNode = NewServerNode()
	return defaultNode
}
