package Cluster

import (
	"fmt"
	ecs2 "gitee.com/alexzjs/project-game/core/ecs"
	"gitee.com/alexzjs/project-game/utils"
	"gitee.com/alexzjs/project-game/utils/config"
	"gitee.com/alexzjs/project-game/utils/logger"
	"reflect"
	"strings"
	"sync"
	"time"
)

const (
	LOG_TYPE_NODE_CLOSE = iota
)

type MasterComponent struct {
	ecs2.ComponentBase
	locker          *sync.RWMutex
	nodeComponent   *NodeComponent
	Nodes           map[string]*NodeInfo
	NodeLog         *NodeLogs
	timeoutChecking map[string]int
}

func (ts *MasterComponent) GetRequire() map[*ecs2.Object][]reflect.Type {
	requires := make(map[*ecs2.Object][]reflect.Type)
	requires[ts.Parent().Root()] = []reflect.Type{
		reflect.TypeOf(&config.Component{}),
		reflect.TypeOf(&NodeComponent{}),
	}
	return requires
}

func (ts *MasterComponent) Awake(ctx *ecs2.Context) {
	ts.locker = &sync.RWMutex{}
	ts.Nodes = make(map[string]*NodeInfo)
	ts.NodeLog = &NodeLogs{BufferSize: 20}
	ts.timeoutChecking = make(map[string]int)

	err := ts.Parent().Root().Find(&ts.nodeComponent)
	if err != nil {
		panic(err)
	}

	//注册Master服务
	s := new(MasterService)
	s.init(ts)
	err = ts.nodeComponent.Register(s)
	if err != nil {
		panic(err)
	}
	if !config.Config.CommonConfig.Debug || false {
		go ts.TimeoutCheck()
	}
}

//上报节点信息
func (ts *MasterComponent) UpdateNodeInfo(args *NodeInfo) {
	ts.locker.Lock()
	if _, ok := ts.Nodes[args.Address]; !ok {
		s := strings.Builder{}
		for _, value := range args.Role {
			s.WriteString(value)
			s.WriteString("  ")
		}
		logger.Info(fmt.Sprintf("Node [ %s ] connected to this master, roles: [ %s]", args.Address, s.String()))
	}
	args.Time = time.Now().UnixNano()
	ts.Nodes[args.Address] = args
	ts.timeoutChecking[args.Address] = 0

	ts.locker.Unlock()
}

//节点主动关闭
func (ts *MasterComponent) NodeClose(addr string) {
	//非线程安全，外层注意加锁
	if v, ok := ts.Nodes[addr]; ok {
		s := strings.Builder{}
		for _, value := range v.Role {
			s.WriteString(value)
			s.WriteString("  ")
		}
		logger.Info(fmt.Sprintf("Node [ %s ] disconnected, roles: [ %s]", addr, s.String()))
	}
	delete(ts.Nodes, addr)
	delete(ts.timeoutChecking, addr)
	ts.NodeLog.Add(&NodeLog{
		Time: time.Now().UnixNano(),
		Type: LOG_TYPE_NODE_CLOSE,
		Log:  addr,
	})
}

//查询节点信息 args : "AppID:Role:SelectorType"
func (ts *MasterComponent) NodeInquiry(args []string, detail bool) ([]*InquiryReply, error) {
	return Selector(ts.Nodes).DoQuery(args, detail, ts.locker)
}

//检查超时节点
func (ts *MasterComponent) TimeoutCheck() map[string]*NodeInfo {
	var interval = time.Duration(config.Config.ClusterConfig.ReportInterval)
	for {
		time.Sleep(time.Millisecond * interval)
		ts.locker.Lock()
		for addr, count := range ts.timeoutChecking {
			ts.timeoutChecking[addr] = count + 1
			if ts.timeoutChecking[addr] > 3 {
				ts.NodeClose(addr)
			}
		}
		ts.locker.Unlock()
	}
}

//深度复制节点信息
func (ts *MasterComponent) NodesCopy() map[string]*NodeInfo {
	ts.locker.RLock()
	defer ts.locker.RUnlock()
	return utils.Copy(ts.Nodes).(map[string]*NodeInfo)
}

func (ts *MasterComponent) NodesLogsCopy() *NodeLogs {
	ts.locker.RLock()
	defer ts.locker.RUnlock()
	return utils.Copy(ts.NodeLog).(*NodeLogs)
}
