package Cluster

import (
	"errors"
)

type NodeInfo struct {
	Time    int64
	Address string
	Role    []string
	AppName string
	Info    map[string]float32
}

type InquiryReply struct {
	Node string
	Info map[string]float32
}

type MasterService struct {
	master *MasterComponent
}

func (ts *MasterService) init(master *MasterComponent) {
	ts.master = master
}

func (ts *MasterService) ReportNodeClose(addr string, reply *bool) error {
	ts.master.locker.Lock()
	ts.master.NodeClose(addr)
	ts.master.locker.Unlock()
	*reply = true
	return nil
}

func (ts *MasterService) ReportNodeInfo(args *NodeInfo, reply *bool) error {
	ts.master.UpdateNodeInfo(args)
	*reply = true
	return nil
}

func (ts *MasterService) NodeInquiry(args []string, reply *[]*InquiryReply) error {
	//logger.Debug("Inquiry :",args)
	res, err := ts.master.NodeInquiry(args, false)
	*reply = res
	return err
}

func (ts *MasterService) NodeInquiryDetail(args []string, reply *[]*InquiryReply) error {
	res, err := ts.master.NodeInquiry(args, true)
	*reply = res
	return err
}

type NodeInfoSyncReply struct {
	Nodes   map[string]*NodeInfo
	NodeLog *NodeLogs
}

func (ts *MasterService) NodeInfoSync(args string, reply *NodeInfoSyncReply) error {
	if args != "sync" {
		return errors.New("call service [ NodeInfoSynchronous ],has wrong argument")
	}
	*reply = NodeInfoSyncReply{
		Nodes:   ts.master.NodesCopy(),
		NodeLog: ts.master.NodesLogsCopy(),
	}
	return nil
}

type NodeLog struct {
	Time int64
	Log  string
	Type int
}

type NodeLogs struct {
	BufferSize int
	Logs       []*NodeLog
}

func (ts *NodeLogs) Add(log *NodeLog) {
	if len(ts.Logs) < ts.BufferSize {
		ts.Logs = append(ts.Logs, log)
	} else {
		ts.Logs = append(ts.Logs[1:], log)
	}
}

func (ts *NodeLogs) Get(time int64) []*NodeLog {
	for key, value := range ts.Logs {
		if value.Time > time {
			return ts.Logs[key:]
		}
	}
	return []*NodeLog{}
}
