package Cluster

import (
	"errors"
	"gitee.com/alexzjs/project-game/utils/rpc"
	"math/rand"
)

type NodeID struct {
	Addr          string
	nodeComponent *NodeComponent
}

func (ts *NodeID) GetClient() (*rpc.TcpClient, error) {
	if ts.Addr == "" {
		return nil, errors.New("this node id is empty")
	}
	return ts.nodeComponent.GetNodeClient(ts.Addr)
}

//无需加锁，只读
type NodeIDGroup struct {
	nodeComponent *NodeComponent
	nodes         []*InquiryReply
}

func NewNodeIDGrop() *NodeIDGroup {
	return &NodeIDGroup{nodes: []*InquiryReply{}}
}

//所有节点，仅地址
func (ts *NodeIDGroup) Nodes() []string {
	nodes := make([]string, len(ts.nodes))
	for _, v := range ts.nodes {
		nodes = append(nodes, v.Node)
	}
	return nodes
}

//所有节点，详细信息
func (ts *NodeIDGroup) NodesDetail() []*InquiryReply {
	return ts.nodes
}

//随机选择一个
func (ts *NodeIDGroup) RandOne() (string, error) {
	if ts.nodes == nil {
		return "", errors.New("this node id group is empty")
	}
	length := len(ts.nodes)
	if length == 0 {
		return "", errors.New("this node id group is empty")
	}
	index := rand.Intn(length)
	return ts.nodes[index].Node, nil
}

//随机选择一个
func (ts *NodeIDGroup) RandClient() (*rpc.TcpClient, error) {
	length := len(ts.nodes)
	if length == 0 {
		return nil, errors.New("this node id group is empty")
	}
	index := rand.Intn(length)

	return ts.nodeComponent.GetNodeClient(ts.nodes[index].Node)
}

//所有客户端
func (ts *NodeIDGroup) Clients() ([]*rpc.TcpClient, error) {
	length := len(ts.nodes)
	if length == 0 {
		return nil, errors.New("this node id group is empty")
	}
	clients := []*rpc.TcpClient{}
	for _, nodeID := range ts.nodes {
		client, err := ts.nodeComponent.GetNodeClient(nodeID.Node)
		if err != nil {
			continue
		}
		clients = append(clients, client)

	}
	if len(clients) <= 0 {
		return nil, errors.New("this node id group is empty")
	}
	return clients, nil
}

//选择一个负载最低的节点
func (ts *NodeIDGroup) MinLoadClient() (*rpc.TcpClient, error) {
	if len(ts.nodes) == 0 {
		return nil, errors.New("this node id group is empty")
	}
	index := SourceGroup(ts.nodes).SelectMinLoad()
	return ts.nodeComponent.GetNodeClient(ts.nodes[index].Node)
}
