package Cluster

import (
	"gitee.com/alexzjs/project-game/utils/logger"
)

type LocationService struct {
	location *LocationComponent
}

func (ts *LocationService) init(mlocation *LocationComponent) {
	ts.location = mlocation
}

func (ts *LocationService) NodeInquiry(args []string, reply *[]*InquiryReply) error {
	logger.Debug("Inquiry :", args)
	res, err := ts.location.NodeInquiry(args, false)
	*reply = res
	return err
}

func (ts *LocationService) NodeInquiryDetail(args []string, reply *[]*InquiryReply) error {
	res, err := ts.location.NodeInquiry(args, true)
	*reply = res
	return err
}

func (ts *LocationService) NodeLogInquiry(args int64, reply *[]*NodeLog) error {
	res, err := ts.location.NodeLogInquiry(args)
	*reply = res
	return err
}
