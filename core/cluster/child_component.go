package Cluster

import (
	"fmt"
	ecs2 "gitee.com/alexzjs/project-game/core/ecs"
	"gitee.com/alexzjs/project-game/utils"
	"gitee.com/alexzjs/project-game/utils/config"
	"gitee.com/alexzjs/project-game/utils/logger"
	"gitee.com/alexzjs/project-game/utils/rpc"
	"reflect"
	"strings"
	"sync"
	"time"
)

type ChildComponent struct {
	ecs2.ComponentBase
	locker          sync.RWMutex
	localAddr       string
	rpcMaster       *rpc.TcpClient //master节点
	nodeComponent   *NodeComponent
	reportCollecter []func() (string, float32)
	close           bool
}

func (ts *ChildComponent) GetRequire() map[*ecs2.Object][]reflect.Type {
	requires := make(map[*ecs2.Object][]reflect.Type)
	requires[ts.Parent().Root()] = []reflect.Type{
		reflect.TypeOf(&config.Component{}),
		reflect.TypeOf(&NodeComponent{}),
	}
	return requires
}

func (ts *ChildComponent) Awake(ctx *ecs2.Context) {
	err := ts.Parent().Root().Find(&ts.nodeComponent)
	if err != nil {
		panic(err)
	}

	go ts.ConnectToMaster()
	go ts.DoReport()
}

func (ts *ChildComponent) Destroy(ctx *ecs2.Context) {
	ts.locker.Lock()
	defer ts.locker.Unlock()

	ts.close = true
	ts.ReportClose(ts.localAddr)
}

//上报节点信息
func (ts *ChildComponent) DoReport() {
	utils.When(time.Millisecond*50,
		func() bool {
			ts.locker.RLock()
			defer ts.locker.RUnlock()

			return ts.rpcMaster != nil
		},
		func() bool {
			ts.locker.RLock()
			defer ts.locker.RUnlock()
			return ts.localAddr != ""
		})
	args := &NodeInfo{
		Address: ts.localAddr,
		Role:    config.Config.ClusterConfig.Role,
		AppName: config.Config.ClusterConfig.AppName,
	}
	var reply bool
	var interval = time.Duration(config.Config.ClusterConfig.ReportInterval)
	for {
		reply = false
		ts.locker.RLock()
		if ts.close {
			ts.locker.RUnlock()
			return
		}
		m := make(map[string]float32)
		for _, collector := range ts.reportCollecter {
			f, d := collector()
			m[f] = d
		}
		args.Info = m
		ts.locker.RUnlock()
		if ts.rpcMaster != nil {
			err := ts.rpcMaster.Call("MasterService.ReportNodeInfo", args, &reply)
			if err != nil {

			}
		}
		time.Sleep(time.Millisecond * interval)
	}
}

//增加上报信息
func (ts *ChildComponent) AddReportInfo(field string, collectFunction func() (string, float32)) {
	ts.locker.Lock()
	ts.reportCollecter = append(ts.reportCollecter, collectFunction)
	ts.locker.Unlock()
}

//增加上报节点关闭
func (ts *ChildComponent) ReportClose(addr string) {
	var reply bool
	if ts.rpcMaster != nil {
		_ = ts.rpcMaster.Call("MasterService.ReportNodeClose", addr, &reply)
	}
}

//连接到master
func (ts *ChildComponent) ConnectToMaster() {
	addr := config.Config.ClusterConfig.MasterAddress
	callback := func(event string, data ...interface{}) {
		switch event {
		case "close":
			ts.OnDropped()
		}
	}
	logger.Info(" Looking for master ......")
	var err error
	for {
		ts.locker.Lock()
		ts.rpcMaster, err = ts.nodeComponent.ConnectToNode(addr, callback)
		if err == nil {
			ip := strings.Split(ts.rpcMaster.LocalAddr(), ":")[0]
			port := strings.Split(config.Config.ClusterConfig.LocalAddress, ":")[1]
			ts.localAddr = fmt.Sprintf("%s:%s", ip, port)
			ts.locker.Unlock()
			break
		}
		ts.locker.Unlock()
		time.Sleep(time.Millisecond * 500)
	}

	ts.nodeComponent.Locker().Lock()
	ts.nodeComponent.isOnline = true
	ts.nodeComponent.Locker().Unlock()

	logger.Info(fmt.Sprintf("Connected to master [ %s ]", addr))
}

//当节点掉线
func (ts *ChildComponent) OnDropped() {
	//重新连接 time.Now().Format("2006-01-02T 15:04:05")
	ts.locker.RLock()
	if ts.close {
		ts.locker.RUnlock()
		return
	}
	ts.locker.RUnlock()
	ts.nodeComponent.Locker().Lock()
	ts.nodeComponent.isOnline = false
	ts.nodeComponent.Locker().Unlock()

	logger.Info("Disconnected from master")
	ts.ConnectToMaster()
}
