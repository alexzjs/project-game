package Cluster

import (
	"errors"
	"fmt"
	ecs2 "gitee.com/alexzjs/project-game/core/ecs"
	"gitee.com/alexzjs/project-game/utils/config"
	"gitee.com/alexzjs/project-game/utils/logger"
	rpc2 "gitee.com/alexzjs/project-game/utils/rpc"
	"math/rand"
	"net"
	"reflect"
	"sync"
	"time"
)

var ErrNodeOffline = errors.New("this node is offline")

type NodeComponent struct {
	ecs2.ComponentBase
	locker          sync.RWMutex
	AppName         string
	localIP         string
	Addr            string
	isOnline        bool
	islocationMode  bool
	rpcClient       sync.Map     //RPC客户端集合
	rpcServer       *rpc2.Server //本节点RPC Server
	serverListener  *net.TCPListener
	locationClients []*rpc2.TcpClient //位置服务器集合
	locationGetter  func()
	lockers         sync.Map //[nodeid,locker]
	clientGetting   map[string]int
}

func (ts *NodeComponent) GetRequire() map[*ecs2.Object][]reflect.Type {
	requires := make(map[*ecs2.Object][]reflect.Type)
	requires[ts.Parent().Root()] = []reflect.Type{
		reflect.TypeOf(&config.Component{}),
	}
	return requires
}

func (ts *NodeComponent) Initialize() error {
	logger.Info("NodeComponent init .....")
	ts.AppName = config.Config.ClusterConfig.AppName
	ts.islocationMode = config.Config.ClusterConfig.IsLocationMode
	ts.clientGetting = make(map[string]int)
	//开始本节点RPC服务
	err := ts.StartRpcServer()
	if err != nil {
		//地址占用，立即修改配置文件
		return err
	}
	//初始化位置服务器搜索
	ts.InitLocationServerGetter()
	ts.locationGetter()
	logger.Info("NodeComponent initialized.")
	return nil
}

func (ts *NodeComponent) Locker() *sync.RWMutex {
	return &ts.locker
}

func (ts *NodeComponent) IsOnline() bool {
	ts.locker.RLock()
	defer ts.locker.RUnlock()

	return ts.isOnline
}

//获取位置服务器
func (ts *NodeComponent) InitLocationServerGetter() {
	if !config.Config.ClusterConfig.IsLocationMode {
		return
	}
	locker := sync.RWMutex{}
	isGetting := false
	//保证同时只有一个在执行
	getter := func() {
		locker.RLock()
		if isGetting {
			locker.RUnlock()
			return
		}
		locker.RUnlock()

		locker.Lock()
		isGetting = true
		locker.Unlock()

		for {
			if !ts.IsOnline() {
				time.Sleep(time.Second)
				continue
			}
			g, err := ts.GetNodeGroupFromMaster("location")
			if err != nil || len(g.Nodes()) == 0 {
				time.Sleep(time.Second)
				continue
			}
			cs, err := g.Clients()
			if err != nil {
				time.Sleep(time.Second)
				continue
			}
			ts.locker.Lock()
			ts.locationClients = cs
			ts.locker.Unlock()
			locker.Lock()
			isGetting = false
			locker.Unlock()
			break
		}
	}
	ts.locationGetter = func() {
		go getter()
	}
	//非频繁更新
	go func() {
		for {
			time.Sleep(time.Second * 30)
			ts.locationGetter()
		}
	}()
}

func (ts *NodeComponent) locationBroken() {
	ts.locker.Lock()
	ts.locationClients = nil
	ts.locker.Unlock()
	ts.locationGetter()
}

//RPC服务
func (ts *NodeComponent) StartRpcServer() error {
	addr, err := net.ResolveTCPAddr("tcp", config.Config.ClusterConfig.LocalAddress)
	if err != nil {
		return err
	}
	server := rpc2.NewServer()
	ts.serverListener, err = net.ListenTCP("tcp", addr)
	if err != nil {
		return err
	}
	ts.rpcServer = server
	ts.localIP = config.Config.ClusterConfig.LocalAddress
	logger.Info(fmt.Sprintf("NodeComponent RPC server listening on: [ %s ]", addr.String()))
	go server.Accept(ts.serverListener)
	return nil
}

func (ts *NodeComponent) Register(rcvr interface{}) error {
	return ts.rpcServer.Register(rcvr)
}

func (ts *NodeComponent) clientCallback(event string, data ...interface{}) {
	switch event {
	case "close":
		nodeAddr := data[0].(string)
		ts.rpcClient.Delete(nodeAddr)
		logger.Info(fmt.Sprintf("  disconnect to remote node: [ %s ]", nodeAddr))
	}
}

//获取节点客户端
func (ts *NodeComponent) GetNodeClient(addr string) (*rpc2.TcpClient, error) {
a:
	if v, ok := ts.rpcClient.Load(addr); ok {
		client := v.(*rpc2.TcpClient)
		if !client.IsClosed() {
			return client, nil
		}
	}

	ts.locker.Lock()
	count, ok := ts.clientGetting[addr]
	if ok {
		if count > 100 {
			ts.locker.Unlock()
			return nil, errors.New("init not complete")
		} else {
			ts.clientGetting[addr] += 1
		}
		ts.locker.Unlock()
		time.Sleep(time.Millisecond * 100)
		goto a
	} else {
		ts.clientGetting[addr] = 0
		ts.locker.Unlock()
	}

	defer func() {
		ts.locker.Lock()
		delete(ts.clientGetting, addr)
		ts.locker.Unlock()
	}()

	client, err := ts.ConnectToNode(addr, ts.clientCallback)
	if err != nil {
		return nil, err
	}
	ts.rpcClient.Store(addr, client)
	return client, nil
}

//查询并选择一个节点
func (ts *NodeComponent) GetNode(role string, selectorType ...SelectorType) (*NodeID, error) {
	var nodeID *NodeID
	var err error
	//优先查询位置服务器
	if ts.islocationMode {
		nodeID, err = ts.GetNodeFromLocation(role, selectorType...)
		if err == nil {
			return nodeID, nil
		}
	}
	//位置服务器不存在或不可用时在master上查询
	nodeID, err = ts.GetNodeFromMaster(role, selectorType...)
	if err != nil {
		return nil, err
	}
	return nodeID, nil
}

//查询获取客户端
func (ts *NodeComponent) GetNodeClientByRole(role string, selectorType ...SelectorType) (*rpc2.TcpClient, error) {
	nodeID, err := ts.GetNode(role, selectorType...)
	if err != nil {
		return nil, err
	}
	client, err := nodeID.GetClient()
	if err != nil {
		return nil, err
	}
	return client, nil
}

//查询节点组
func (ts *NodeComponent) GetNodeGroup(role string) (*NodeIDGroup, error) {
	var nodeIDGroup *NodeIDGroup
	var err error
	//优先查询位置服务器
	if ts.islocationMode {
		nodeIDGroup, err = ts.GetNodeGroupFromLocation(role)
		if err == nil {
			return nodeIDGroup, nil
		}
	}

	//位置服务器不存在或不可用时在master上查询
	nodeIDGroup, err = ts.GetNodeGroupFromMaster(role)
	if err != nil {
		return nil, err
	}

	if nodeIDGroup == nil {
		nodeIDGroup = NewNodeIDGrop()
	}
	return nodeIDGroup, nil
}

//从位置服务器查询并选择一个节点
func (ts *NodeComponent) GetNodeFromLocation(role string, selectorType ...SelectorType) (*NodeID, error) {
	var client *rpc2.TcpClient
	var err error

	ts.locker.RLock()
	if ts.locationClients == nil {
		ts.locker.RUnlock()
		return nil, errors.New("location server not found")
	}
	//随机一个节点
	rnd := rand.Intn(len(ts.locationClients))
	client = ts.locationClients[rnd]
	ts.locker.RUnlock()

	var reply *[]*InquiryReply
	args := []string{
		SELECTOR_TYPE_DEFAULT, config.Config.ClusterConfig.AppName, role,
	}
	if len(selectorType) > 0 {
		args[0] = selectorType[0]
	}

	err = client.Call("LocationService.NodeInquiry", args, &reply)
	if err != nil {
		ts.locationBroken()
		return nil, err
	}
	if len(*reply) > 0 {
		g := &NodeID{
			nodeComponent: ts,
			Addr:          (*reply)[0].Node,
		}
		return g, nil
	}

	return nil, errors.New("no node of this role:" + role)
}

//从位置服务器查询
func (ts *NodeComponent) GetNodeGroupFromLocation(role string) (*NodeIDGroup, error) {
	var client *rpc2.TcpClient
	var err error

	ts.locker.RLock()
	if ts.locationClients == nil {
		ts.locker.RUnlock()
		return nil, errors.New("location server not found")
	}
	//随机一个节点
	rnd := rand.Intn(len(ts.locationClients))
	client = ts.locationClients[rnd]
	ts.locker.RUnlock()

	var reply *[]*InquiryReply
	args := []string{
		SELECTOR_TYPE_GROUP, config.Config.ClusterConfig.AppName, role,
	}
	err = client.Call("LocationService.NodeInquiry", args, &reply)
	if err != nil {
		ts.locationBroken()
		return nil, err
	}
	g := &NodeIDGroup{
		nodeComponent: ts,
		nodes:         *reply,
	}
	return g, nil
}

//从master查询并选择一个节点
func (ts *NodeComponent) GetNodeFromMaster(role string, selectorType ...SelectorType) (*NodeID, error) {
	if !ts.IsOnline() {
		return nil, ErrNodeOffline
	}
	client, err := ts.GetNodeClient(config.Config.ClusterConfig.MasterAddress)
	if err != nil {
		return nil, err
	}
	var reply *[]*InquiryReply
	args := []string{
		SELECTOR_TYPE_DEFAULT, config.Config.ClusterConfig.AppName, role,
	}
	if len(selectorType) > 0 {
		args[0] = selectorType[0]
	}
	err = client.Call("MasterService.NodeInquiry", args, &reply)
	if err != nil {
		return nil, err
	}
	if len(*reply) > 0 {
		g := &NodeID{
			nodeComponent: ts,
			Addr:          (*reply)[0].Node,
		}
		return g, nil
	}

	return nil, errors.New("no node of this role:" + role)
}

//从master查询
func (ts *NodeComponent) GetNodeGroupFromMaster(role string) (*NodeIDGroup, error) {
	if !ts.IsOnline() {
		return nil, ErrNodeOffline
	}
	client, err := ts.GetNodeClient(config.Config.ClusterConfig.MasterAddress)
	if err != nil {
		return nil, err
	}
	var reply *[]*InquiryReply
	args := []string{
		SELECTOR_TYPE_GROUP, config.Config.ClusterConfig.AppName, role,
	}
	err = client.Call("MasterService.NodeInquiry", args, &reply)
	if err != nil {
		return nil, err
	}
	g := &NodeIDGroup{
		nodeComponent: ts,
		nodes:         *reply,
	}
	return g, nil
}

//连接到某个节点
func (ts *NodeComponent) ConnectToNode(addr string, callback func(event string, data ...interface{})) (*rpc2.TcpClient, error) {
	client, err := rpc2.NewTcpClient("tcp", addr, callback)
	if err != nil {
		return nil, err
	}

	ts.rpcClient.Store(addr, client)

	logger.Info(fmt.Sprintf("  connect to node: [ %s ] success", addr))
	return client, nil
}
