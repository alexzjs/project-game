package Cluster

import (
	"errors"
	ecs2 "gitee.com/alexzjs/project-game/core/ecs"
	"gitee.com/alexzjs/project-game/utils/config"
	"gitee.com/alexzjs/project-game/utils/rpc"
	"reflect"
	"sync"
	"time"
)

type LocationReply struct {
	NodeNetAddress map[string]string //[node id , ip]
}
type LocationQuery struct {
	Group  string
	AppID  string
	NodeID string
}

type LocationComponent struct {
	ecs2.ComponentBase
	locker        *sync.RWMutex
	nodeComponent *NodeComponent
	Nodes         map[string]*NodeInfo
	NodeLog       *NodeLogs
	master        *rpc.TcpClient
}

func (ts *LocationComponent) GetRequire() map[*ecs2.Object][]reflect.Type {
	requires := make(map[*ecs2.Object][]reflect.Type)
	requires[ts.Runtime().Root()] = []reflect.Type{
		reflect.TypeOf(&config.Component{}),
		reflect.TypeOf(&NodeComponent{}),
	}
	return requires
}

func (ts *LocationComponent) Awake(ctx *ecs2.Context) {
	ts.locker = &sync.RWMutex{}
	err := ts.Parent().Root().Find(&ts.nodeComponent)
	if err != nil {
		panic(err)
	}

	//注册位置服务节点RPC服务
	service := new(LocationService)
	service.init(ts)
	err = ts.nodeComponent.Register(service)
	if err != nil {
		panic(err)
	}
	go ts.DoLocationSync()
}

//同步节点信息到位置服务组件
func (ts *LocationComponent) DoLocationSync() {
	var reply *NodeInfoSyncReply
	var interval = time.Duration(config.Config.ClusterConfig.LocationSyncInterval)
	for {
		if ts.master == nil {
			var err error
			ts.master, err = ts.nodeComponent.GetNodeClient(config.Config.ClusterConfig.MasterAddress)
			if err != nil {
				time.Sleep(time.Second * interval)
				continue
			}
		}
		err := ts.master.Call("MasterService.NodeInfoSync", "sync", &reply)
		if err != nil {
			ts.master = nil
			continue
		}
		ts.locker.Lock()
		ts.Nodes = reply.Nodes
		ts.NodeLog = reply.NodeLog
		ts.locker.Unlock()
		time.Sleep(time.Millisecond * interval)
	}
}

//查询节点信息 args : "AppID:Role:SelectorType"
func (ts *LocationComponent) NodeInquiry(args []string, detail bool) ([]*InquiryReply, error) {
	if ts.Nodes == nil {
		return nil, errors.New("this location node is waiting to sync")
	}
	return Selector(ts.Nodes).DoQuery(args, detail, ts.locker)
}

//日志获取
func (ts *LocationComponent) NodeLogInquiry(args int64) ([]*NodeLog, error) {
	ts.locker.RLock()
	defer ts.locker.RUnlock()

	if ts.NodeLog == nil {
		return nil, errors.New("this location node is waiting to sync")
	}
	return ts.NodeLog.Get(args), nil
}
