package ecs_test

import (
	ecs2 "gitee.com/alexzjs/project-game/core/ecs"
	"reflect"
)

type ComponentTemplate struct {
	ecs2.ComponentBase
	parent *ecs2.Object
}

func (c *ComponentTemplate) New() ecs2.IComponent {
	return &ComponentTemplate{}
}

func (c *ComponentTemplate) Type() reflect.Type {
	return reflect.TypeOf(c)
}

func (c *ComponentTemplate) Awake(parent *ecs2.Object) {
	c.parent = parent
}

func (c *ComponentTemplate) Update(context *ecs2.Context) {
}

func (c *ComponentTemplate) Serialize() (interface{}, error) {
	return "", nil
}

func (c *ComponentTemplate) Deserialize(raw interface{}) error {
	return nil
}
