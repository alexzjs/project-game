package ecs

import (
	"container/list"
	"sync"
)

const (
	SYSTEM_OP_DESTROY_ADD = iota
	SYSTEM_OP_DESTROY_REMOVE
)

type IDestroy interface {
	Destroy(context *Context)
}

type DestroySystem struct {
	SystemBase
	wg         *sync.WaitGroup
	components *list.List
	temp       *list.List
}

func (ts *DestroySystem) Init(runtime *Runtime) {
	ts.name = "destroy"
	ts.components = list.New()
	ts.temp = list.New()
	ts.wg = &sync.WaitGroup{}
	ts.runtime = runtime
}

func (ts *DestroySystem) Name() string {
	ts.locker.RLock()
	defer ts.locker.RUnlock()
	return ts.name
}

func (ts *DestroySystem) UpdateFrame() {
	ts.locker.Lock()
	ts.components, ts.temp = ts.temp, ts.components
	ts.locker.Unlock()

	for c := ts.temp.Front(); c != nil; c = ts.temp.Front() {
		ts.wg.Add(1)
		ctx := &Context{
			Runtime: ts.runtime,
		}

		v := c.Value.(IDestroy)
		ts.runtime.workers.Run(func() {
			v.Destroy(ctx)
			ts.wg.Done()
		})
		ts.temp.Remove(c)
	}
	ts.wg.Wait()
}

func (ts *DestroySystem) Filter(component IComponent) {

}

func (ts *DestroySystem) IndependentFilter(op int, component IComponent) {
	ts.locker.Lock()
	defer ts.locker.Unlock()

	s, ok := component.(IDestroy)
	if ok {
		ts.components.PushBack(s)
	}
}
