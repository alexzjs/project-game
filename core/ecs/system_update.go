package ecs

import (
	"container/list"
	"gitee.com/alexzjs/project-game/utils/3rd/iter"
	"sync"
)

const (
	SYSTEM_OP_UPDATE_ADD = iota
	SYSTEM_OP_UPDATE_REMOVE
)

type IUpdate interface {
	Update(context *Context)
}

type UpdateSystem struct {
	SystemBase
	wg         *sync.WaitGroup
	components *list.List
	push       *list.List
	pop        *list.List
}

func (ts *UpdateSystem) Init(runtime *Runtime) {
	ts.name = "update"
	ts.components = list.New()
	ts.push = list.New()
	ts.pop = list.New()
	ts.wg = &sync.WaitGroup{}
	ts.runtime = runtime
}

func (ts *UpdateSystem) Name() string {
	ts.locker.RLock()
	defer ts.locker.RUnlock()
	return ts.name
}

func (ts *UpdateSystem) UpdateFrame() {
	ts.locker.Lock()
	//添加上一帧新添加的组件
	if ts.push.Len() > 0 {
		ts.components.PushBackList(ts.push)
		ts.push = list.New()
	}
	//移除上一帧删除的组件，添加在前，移除在后，同一帧添加又移除，不执行update
	if ts.pop.Len() > 0 {
		for p := ts.pop.Front(); p != nil; p = p.Next() {
			for c := ts.components.Front(); c != nil; c = c.Next() {
				if c.Value == p.Value {
					ts.components.Remove(c)
				}
			}
		}
		ts.pop = list.New()
	}
	ts.locker.Unlock()

	Iter := iter.FromList(ts.components)
	for c, err := Iter.Next(); err == nil; c, err = Iter.Next() {
		ts.wg.Add(1)
		ctx := &Context{
			Runtime: ts.runtime,
		}
		v := c.(IUpdate)
		ts.runtime.workers.Run(func() {
			v.Update(ctx)
			ts.wg.Done()
		})
	}
	ts.wg.Wait()
}

func (ts *UpdateSystem) Filter(component IComponent) {
	s, ok := component.(IUpdate)
	if ok {
		ts.locker.Lock()
		ts.push.PushBack(s)
		ts.locker.Unlock()
	}
}

func (ts *UpdateSystem) IndependentFilter(op int, component IComponent) {
	switch op {
	case SYSTEM_OP_UPDATE_ADD:
		ts.Filter(component)
	case SYSTEM_OP_UPDATE_REMOVE:
		s, ok := component.(IUpdate)
		if !ok {
			return
		}
		ts.locker.Lock()
		ts.pop.PushBack(s)
		ts.locker.Unlock()
	}
}
