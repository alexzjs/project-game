package ecs_test

import (
	ecs2 "gitee.com/alexzjs/project-game/core/ecs"
	"reflect"
)

type FakeConfiguredComponentData struct {
	Items []FakeConfiguredComponentItem
}

type FakeConfiguredComponentItem struct {
	Id    string
	Count int
}

type FakeConfiguredComponent struct {
	ecs2.ComponentBase
	Data FakeConfiguredComponentData
}

func (fake *FakeConfiguredComponent) Type() reflect.Type {
	return reflect.TypeOf(fake)
}

func (fake *FakeConfiguredComponent) New() ecs2.IComponent {
	return &FakeConfiguredComponent{}
}

func (fake *FakeConfiguredComponent) Serialize() (interface{}, error) {
	return ecs2.SerializeState(&fake.Data)
}

func (fake *FakeConfiguredComponent) Deserialize(raw interface{}) error {
	var data FakeConfiguredComponentData
	if err := ecs2.DeserializeState(&data, raw); err != nil {
		return err
	}
	fake.Data = data
	return nil
}
