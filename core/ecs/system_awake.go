package ecs

import (
	"container/list"
	"sync"
)

type IAwake interface {
	Awake(context *Context)
}

type AwakeSystem struct {
	SystemBase
	wg         *sync.WaitGroup
	components *list.List
	temp       *list.List
}

func (ts *AwakeSystem) Init(runtime *Runtime) {
	ts.name = "awake"
	ts.components = list.New()
	ts.temp = list.New()
	ts.wg = &sync.WaitGroup{}
	ts.runtime = runtime
}

func (ts *AwakeSystem) Name() string {
	ts.locker.RLock()
	defer ts.locker.RUnlock()
	return ts.name
}

func (ts *AwakeSystem) UpdateFrame() {
	ts.locker.Lock()
	ts.components, ts.temp = ts.temp, ts.components
	ts.locker.Unlock()

	for c := ts.temp.Front(); c != nil; c = ts.temp.Front() {
		ts.wg.Add(1)
		ctx := &Context{
			Runtime: ts.runtime,
		}

		v := c.Value.(IAwake)
		//name:=c.Value.(IComponent).Type().String()
		ts.runtime.workers.Run(func() {
			//logger.Debug("awake: "+name)
			v.Awake(ctx)
			ts.wg.Done()
		})
		ts.temp.Remove(c)
	}
	ts.wg.Wait()
}

func (ts *AwakeSystem) Filter(component IComponent) {
	ts.locker.Lock()
	defer ts.locker.Unlock()

	s, ok := component.(IAwake)
	if ok {
		ts.components.PushBack(s)
	}
}

func (ts *AwakeSystem) IndependentFilter(op int, component IComponent) {
	ts.Filter(component)
}
