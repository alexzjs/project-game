package ecs

import (
	"container/list"
	"sync"
)

type IStart interface {
	Start(context *Context)
}

type StartSystem struct {
	SystemBase
	wg         *sync.WaitGroup
	components *list.List
	temp       *list.List
}

func (ts *StartSystem) Init(runtime *Runtime) {
	ts.name = "start"
	ts.components = list.New()
	ts.temp = list.New()
	ts.wg = &sync.WaitGroup{}
	ts.runtime = runtime
}

func (ts *StartSystem) Name() string {
	ts.locker.RLock()
	defer ts.locker.RUnlock()
	return ts.name
}

func (ts *StartSystem) UpdateFrame() {
	ts.locker.Lock()
	ts.components, ts.temp = ts.temp, ts.components
	ts.locker.Unlock()

	for c := ts.temp.Front(); c != nil; c = ts.temp.Front() {
		ts.wg.Add(1)
		ctx := &Context{
			Runtime: ts.runtime,
		}

		v := c.Value.(IStart)
		ts.runtime.workers.Run(func() {
			v.Start(ctx)
			ts.wg.Done()
		})
		ts.temp.Remove(c)
	}
	ts.wg.Wait()
}

func (ts *StartSystem) Filter(component IComponent) {
	ts.locker.Lock()
	defer ts.locker.Unlock()

	s, ok := component.(IStart)
	if ok {
		ts.components.PushBack(s)
	}
}
func (ts *StartSystem) IndependentFilter(op int, component IComponent) {
	ts.Filter(component)
}
