package ecs_test

var objectTemplateSimple = `
{
	"Name": "Sample Object",
	"Components": [
		{"Type": "*gitee.com/alexzjs/project-game/Component_test.FakeComponent"},
		{"Type": "*gitee.com/alexzjs/project-game/Component_test.FakeConfiguredComponent", "Data": {
			"Items": [
				{"Id": "1", "Count": 1},
				{"Id": "2", "Count": 2},
				{"Id": "3", "Count": 3}
			]
		}}
	],
	"Objects": [
	]
}
`

var objectTemplateNested = `
{
	"Name": "Sample Object",
	"Components": [{
		"Type": "*gitee.com/alexzjs/project-game/Component_test.FakeComponent"
	}],
	"Objects": [{
			"Name": "N/A",
			"Components": [{
				"Type": "*gitee.com/alexzjs/project-game/Component_test.FakeComponent"
			}],
			"Objects": []
		}, {
			"Name": "One",
			"Objects": [{
				"Name": "Two",
				"Components": [{
					"Type": "*gitee.com/alexzjs/project-game/Component_test.FakeComponent"
				}, {
					"Type": "*gitee.com/alexzjs/project-game/Component_test.FakeConfiguredComponent",
					"Data": {
						"Items": [{
							"Id": "1",
							"Count": 1
						}, {
							"Id": "2",
							"Count": 2
						}, {
							"Id": "3",
							"Count": 3
						}]
					}
				}],
				"Objects": []
			}]
		}
	]
}
`
