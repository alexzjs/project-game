package ecs_test

import (
	ecs2 "gitee.com/alexzjs/project-game/core/ecs"
	"gitee.com/alexzjs/project-game/utils/3rd/assert"
	"gitee.com/alexzjs/project-game/utils/3rd/iter"
	"testing"

	"reflect"
)

func TestGetComponents(T *testing.T) {
	assert.Test(T, func(T *assert.T) {
		obj := ecs2.NewObject("Object 1")
		obj.AddComponent(&FakeComponent{Id: "1"})
		obj.AddComponent(&FakeComponent{Id: "1"})
		os := obj.GetComponents(reflect.TypeOf((ecs2.IComponent)(nil)))
		ci, err := iter.Collect(os)
		t2 := reflect.TypeOf((*FakeComponent)(nil))
		_ = t2
		//ci, err := iter.Collect(obj.GetComponents(reflect.TypeOf((*FakeComponent)(nil))))
		T.Assert(err == nil)
		T.Assert(len(ci) == 1)
		T.Assert(ci[0].(*FakeComponent).Id == "1")
	})
}

func TestGetComponentsInChildren(T *testing.T) {
	assert.Test(T, func(T *assert.T) {
		obj := ecs2.NewObject()
		obj2 := ecs2.NewObject()
		obj3 := ecs2.NewObject()
		obj.AddObject(obj2)
		obj2.AddObject(obj3)

		obj3.AddComponent(&FakeComponent{Id: "1"})
		obj3.AddComponent(&FakeComponent{Id: "2"})

		ci, err := iter.Collect(obj.GetComponents(reflect.TypeOf((*FakeComponent)(nil))))
		T.Assert(err == nil)
		T.Assert(len(ci) == 0)

		ci, err = iter.Collect(obj.GetComponentsInChildren(reflect.TypeOf((*FakeComponent)(nil))))
		T.Assert(err == nil)
		T.Assert(len(ci) == 2)
		T.Assert(ci[0].(*FakeComponent).Id == "1")
		T.Assert(ci[1].(*FakeComponent).Id == "2")
	})
}
