package Actor

import (
	"bytes"
	"errors"
	"fmt"
	"strings"
)

var (
	//actor地址格式错误
	ErrActorWrongFormat = errors.New("ts format is wrong,should be : IP:Port:LocalActorID")
)

type ActorAddressType = int

/*
	Target such as "127.0.0.1:8888:XXXXXXX",
	means "IP:PORT:LOCALATORID"
*/
type ActorID []string

func EmptyActorID() ActorID {
	return make([]string, 3)
}

func (ts ActorID) Equal(target ActorID) bool {
	if len(ts) != len(target) {
		return false
	}
	for i, value := range ts {
		if value != target[i] {
			return false
		}
	}
	return true
}

func (ts ActorID) String() string {
	if len(ts) == 0 {
		return "unknown"
	}
	buf := bytes.Buffer{}
	for index, value := range ts {
		buf.WriteString(value)
		if index != len(ts)-1 {
			buf.WriteString(":")
		}
	}
	return buf.String()
}

func (ts *ActorID) SetNodeID(address string) (ActorID, error) {
	arr := strings.Split(address, ":")
	if len(arr) != 2 {
		return nil, ErrActorWrongFormat
	}
	(*ts)[0] = arr[0]
	(*ts)[1] = arr[1]
	return *ts, nil
}

func (ts *ActorID) SetLocalActorID(id string) *ActorID {
	(*ts)[2] = id
	return ts
}

func (ts *ActorID) Parse(address string) error {
	arr := strings.Split(address, ":")
	if len(arr) != 3 {
		return ErrActorWrongFormat
	}
	*ts = arr
	return nil
}

//get node address
func (ts ActorID) GetNodeID() string {
	return fmt.Sprintf("%s:%s", ts[0], ts[1])
}

//get node address
func (ts ActorID) GetLocalActorID() string {
	return ts[2]
}
