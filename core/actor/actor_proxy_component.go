package Actor

import (
	"errors"
	"fmt"
	"gitee.com/alexzjs/project-game/core/cluster"
	ecs2 "gitee.com/alexzjs/project-game/core/ecs"
	"gitee.com/alexzjs/project-game/utils/config"
	"gitee.com/alexzjs/project-game/utils/logger"
	"gitee.com/alexzjs/project-game/utils/rpc"
	"gitee.com/alexzjs/project-game/utils/uuid"
	"reflect"
	"sync"
)

var ErrNoThisService = errors.New("no this service")
var ErrNoThisActor = errors.New("no this actor")

type ActorProxyComponent struct {
	ecs2.ComponentBase
	locker        sync.RWMutex
	nodeID        string
	localActors   sync.Map //本地actor [Target,actor]
	service       sync.Map // [service,[]actor]
	nodeComponent *Cluster.NodeComponent
	location      *rpc.TcpClient
	//isActorMode   bool
	isOnline bool
}

func (ts *ActorProxyComponent) GetRequire() map[*ecs2.Object][]reflect.Type {
	requires := make(map[*ecs2.Object][]reflect.Type)
	//添加该组件需要根节点拥有ActorProxyComponent,ConfigComponent组件
	requires[ts.Runtime().Root()] = []reflect.Type{
		reflect.TypeOf(&config.Component{}),
	}
	return requires
}

func (ts *ActorProxyComponent) IsUnique() int {
	return ecs2.UNIQUE_TYPE_GLOBAL
}

func (ts *ActorProxyComponent) Initialize() error {
	logger.Info("ActorProxyComponent init .....")
	ts.nodeID = config.Config.ClusterConfig.LocalAddress
	//ts.isActorMode = Config.Config.ClusterConfig.IsActorModel
	err := ts.Runtime().Root().Find(&ts.nodeComponent)
	if err != nil {
		return err
	}
	//注册ActorProxyService服务
	s := new(ActorProxyService)
	s.init(ts)
	err = ts.nodeComponent.Register(s)
	if err != nil {
		return err
	}
	logger.Info("ActorProxyComponent initialized.")
	return nil
}

func (ts *ActorProxyComponent) IsOnline() bool {
	ts.locker.RLock()
	defer ts.locker.RUnlock()
	return ts.isOnline
}

func (ts *ActorProxyComponent) Destroy(ctx *ecs2.Context) {

}

//获取本地actor服务
func (ts *ActorProxyComponent) GetLocalActorService(serviceName string) (*ActorService, error) {
	var service *ActorService
	var err error
	s, ok := ts.service.Load(serviceName)
	if !ok {
		return nil, ErrNoThisService
	}
	service = s.(*ActorService)
	if err != nil {
		return nil, err
	}
	return service, nil
}

//获取actor服务
func (ts *ActorProxyComponent) GetActorService(role string, serviceName string) (*ActorService, error) {
	var service *ActorService
	var err error
	//优先尝试本地服务
	service, err = ts.GetLocalActorService(serviceName)
	if err == nil {
		return service, nil
	}

	//获取远程服务
	if role == LOCAL_SERVICE {
		return nil, errors.New("role is empty")
	}
	client, err := ts.nodeComponent.GetNodeClientByRole(role)
	if err != nil {
		return nil, err
	}
	var reply ActorID
	err = client.Call("ActorProxyService.ServiceInquiry", serviceName, &reply)
	if err != nil {
		return nil, err
	}
	return NewActorService(NewActor(reply, ts), serviceName), nil
}

//注册服务
func (ts *ActorProxyComponent) RegisterService(actor IActor, service string) error {
	_, ok := ts.service.Load(service)
	if ok {
		return errors.New("this service is repeated")
	}
	ts.service.Store(service, NewActorService(actor, service))
	return nil
}

//取消注册服务
func (ts *ActorProxyComponent) UnregisterService(service string) {
	ts.service.Delete(service)
}

//注册本地actor
func (ts *ActorProxyComponent) Register(actor IActor) error {
	id := actor.ID()
	id[2] = uuid.Next()
	id, err := id.SetNodeID(ts.nodeID)
	if err != nil {
		return err
	}
	ts.localActors.Store(id.String(), actor)
	return nil
}

//注销本地actor
func (ts *ActorProxyComponent) Unregister(actor IActor) {
	if _, ok := ts.localActors.Load(actor.ID().String()); ok {
		ts.localActors.Delete(actor.ID().String())
		return
	}
}

//发送本地消息
func (ts *ActorProxyComponent) LocalTell(actorID ActorID, messageInfo *ActorMessageInfo) error {
	v, ok := ts.localActors.Load(actorID.String())
	if !ok {
		return ErrNoThisActor
	}
	actor, ok := v.(IActor)
	if !ok {
		return ErrNoThisActor
	}
	return actor.Tell(messageInfo.Sender, messageInfo.Message, messageInfo.reply)
}

//通过actor id 发送消息
func (ts *ActorProxyComponent) Emit(actorID ActorID, messageInfo *ActorMessageInfo) error {
	senderID := "unknown"
	if messageInfo.Sender != nil {
		senderID = messageInfo.Sender.ID().String()
	}
	logger.Debug(fmt.Sprintf("actor: [ %s ] send message [ %s ] to actor [ %s ]", senderID, messageInfo.Message.Service, actorID.String()))
	nodeID := actorID.GetNodeID()

	//本地消息不走网络
	if nodeID == ts.nodeID {
		return ts.LocalTell(actorID, messageInfo)
	}
	//非本地消息走网络代理
	client, err := ts.nodeComponent.GetNodeClient(nodeID)
	if err != nil {
		return err
	}
	var sender ActorID
	if messageInfo.Sender != nil {
		sender = messageInfo.Sender.ID()
	}
	err = client.Call("ActorProxyService.Tell", &ActorRpcMessageInfo{
		Target:  actorID,
		Sender:  sender,
		Message: messageInfo.Message}, messageInfo.reply)
	if err != nil {
		return err
	}
	return nil
}
