package Actor

type ActorProxyService struct {
	proxy *ActorProxyComponent
}

func (ts *ActorProxyService) init(proxy *ActorProxyComponent) {
	ts.proxy = proxy
}
func (ts *ActorProxyService) Tell(args *ActorRpcMessageInfo, reply *ActorMessage) error {
	minfo := &ActorMessageInfo{
		Sender:  NewActor(args.Sender, ts.proxy),
		Message: args.Message,
		reply:   &reply,
	}
	return ts.proxy.Emit(args.Target, minfo)
}

func (ts *ActorProxyService) ServiceInquiry(service string, reply *ActorID) error {
	r, err := ts.proxy.GetLocalActorService(service)
	if err != nil {
		return err
	}
	*reply = r.actor.ID()
	return nil
}
