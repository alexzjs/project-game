package Actor

import (
	"gitee.com/alexzjs/project-game/core/ecs"
	"gitee.com/alexzjs/project-game/utils/logger"
)

type ActorBase struct {
	MessageHandler map[string]func(message *ActorMessageInfo) error
	actor          *ActorComponent
	actorType      ActorType
	parent         *ecs.Object
}

func (ts *ActorBase) ActorInit(parent *ecs.Object, actorType ...ActorType) {
	ts.parent = parent
	if len(actorType) == 0 {
		ts.actorType = ACTOR_TYPE_DEFAULT
	} else {
		ts.actorType = actorType[0]
	}
}

func (ts *ActorBase) Actor() *ActorComponent {
	ts.panic()
	if ts.actor == nil && ts.parent != nil {
		err := ts.parent.Find(&ts.actor)
		if err != nil {
			actor := NewActorComponent(ts.actorType)
			ts.parent.AddComponent(actor)
			ts.actor = actor
		}
	}
	return ts.actor
}

func (ts *ActorBase) MessageHandlers() map[string]func(message *ActorMessageInfo) error {
	ts.panic()
	return ts.MessageHandler
}

func (ts *ActorBase) AddHandler(service string, handler func(message *ActorMessageInfo) error, isService ...bool) {
	ts.panic()
	if ts.MessageHandler == nil {
		ts.MessageHandler = map[string]func(message *ActorMessageInfo) error{}
	}
	ts.MessageHandler[service] = handler
	actor := ts.Actor()
	if actor != nil && len(isService) > 0 {
		err := actor.RegisterService(service)
		if err != nil {
			logger.Error(err)
		}
	}
}

func (ts *ActorBase) panic() {
	if ts.parent == nil {
		panic("actor base must be initialized first")
	}
}
