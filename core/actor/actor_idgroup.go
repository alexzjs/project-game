package Actor

import (
	"errors"
	"math/rand"
	"sync"
)

type ActorIDGroup struct {
	locker sync.RWMutex
	Actors []ActorID
}

func (ts ActorIDGroup) isRepeated(target ActorID) bool {
	//外层注意加锁

	for _, value := range ts.Actors {
		if value.Equal(target) {
			return true
		}
	}
	return false
}

func (ts *ActorIDGroup) Add(id ActorID) {
	ts.locker.Lock()
	defer ts.locker.Unlock()

	if !ts.isRepeated(id) {
		ts.Actors = append(ts.Actors, id)
	}
}

func (ts *ActorIDGroup) Sub(id ActorID) {
	ts.locker.Lock()
	defer ts.locker.Unlock()

	for i, value := range ts.Actors {
		if value.Equal(id) {
			ts.Actors = append(ts.Actors[:i], ts.Actors[i+1:]...)
			return
		}
	}
}

func (ts *ActorIDGroup) Has(id ActorID) bool {
	ts.locker.RLock()
	defer ts.locker.RUnlock()

	for _, value := range ts.Actors {
		if value.Equal(id) {
			return true
		}
	}
	return false
}

func (ts *ActorIDGroup) Get() []ActorID {
	ts.locker.RLock()
	defer ts.locker.RUnlock()

	as := make([]ActorID, 0, len(ts.Actors))
	copy(as, ts.Actors)
	return as
}

func (ts *ActorIDGroup) RndOne() (ActorID, error) {
	ts.locker.RLock()
	defer ts.locker.RUnlock()
	l := len(ts.Actors)
	if l == 0 {
		return nil, errors.New("this actor id group is empty")
	}
	r := rand.Intn(l)
	return ts.Actors[r], nil
}
