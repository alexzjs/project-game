package Actor

type ActorService struct {
	actor   IActor
	Service string
}

func NewActorService(actor IActor, service string) *ActorService {
	return &ActorService{actor: actor, Service: service}
}

func (ts *ActorService) Call(args ...interface{}) ([]interface{}, error) {
	mes := NewActorMessage(ts.Service, args...)
	reply := &ActorMessage{}
	err := ts.actor.Tell(nil, mes, &reply)
	if err != nil {
		return nil, err
	}
	return reply.Data, nil
}
