package Actor

import (
	"errors"
)

/*
	ActorMessage Information
*/

var ErrTimeout = errors.New("actor tell time out")

type ActorMessageInfo struct {
	Sender  IActor
	Message *ActorMessage
	reply   **ActorMessage
	done    chan struct{}
	isDone  bool
	err     error
}

func (ts *ActorMessageInfo) NeedReply(isReply bool) {
	if isReply {
		ts.done = make(chan struct{})
	}
}

func (ts *ActorMessageInfo) IsNeedReply() bool {
	return ts.done != nil
}

func (ts *ActorMessageInfo) Reply(args ...interface{}) error {
	return ts.ReplyWithService("", args...)
}

func (ts *ActorMessageInfo) ReplyWithService(service string, args ...interface{}) error {
	if ts.done != nil {
		**ts.reply = ActorMessage{
			Service: service,
			Data:    args,
		}
		return nil
	} else {
		return errors.New("this message invalid")
	}
}

func (ts *ActorMessageInfo) replyError(err error) {
	ts.err = err
	if ts.done != nil && !ts.isDone {
		ts.done <- struct{}{}
		ts.isDone = true
	}
}

func (ts *ActorMessageInfo) replySuccess() {
	if ts.done != nil && !ts.isDone {
		ts.done <- struct{}{}
		ts.isDone = true
	}
}

type ActorRpcMessageInfo struct {
	Target  ActorID
	Sender  ActorID
	Message *ActorMessage
}
