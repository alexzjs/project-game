package Actor

import (
	"errors"
	network2 "gitee.com/alexzjs/project-game/utils/network"
	"sync"
)

type ActorWithSession struct {
	locker  sync.RWMutex
	actorID ActorID
	proxy   *ActorProxyComponent
	session *network2.Session
	api     network2.NetAPI
}

func NewActorWithSession(proxy *ActorProxyComponent, sess *network2.Session) (*ActorWithSession, error) {
	actor := &ActorWithSession{actorID: EmptyActorID(), proxy: proxy, session: sess}
	err := proxy.Register(actor)
	if err != nil {
		return nil, err
	}
	actor.session.AddPostProcessing(func(sess *network2.Session) {
		proxy.Unregister(actor)
	})

	return actor, nil
}

func (ts *ActorWithSession) ID() ActorID {
	return ts.actorID
}

func (ts *ActorWithSession) Tell(sender IActor, message *ActorMessage, reply ...**ActorMessage) error {
	if len(message.Data) == 0 {
		return errors.New("invalid message")
	}
	ts.api.Reply(ts.session, message.Data[0])
	return nil
}
