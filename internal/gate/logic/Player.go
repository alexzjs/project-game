package logic

import Actor "gitee.com/alexzjs/project-game/core/actor"

type Player struct {
	UID  int         //玩家ID
	Info *PlayerInfo //玩家信息
	addr Actor.IActor
}
