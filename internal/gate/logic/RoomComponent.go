package logic

import (
	"errors"
	Actor "gitee.com/alexzjs/project-game/core/actor"
	"gitee.com/alexzjs/project-game/core/ecs"
)

type RoomComponent struct {
	ecs.ComponentBase
	Actor.ActorBase
	players map[int]*Player
	RoomID  int
}

func (ts *RoomComponent) Initialize() error {
	ts.ActorInit(ts.Parent(), Actor.ACTOR_TYPE_SYNC)
	return nil
}

func (ts *RoomComponent) Awake(ctx *ecs.Context) {

}

var ErrLoginPlayerNotExist = errors.New("this player doesnt exist")

func (ts *RoomComponent) Enter(player *Player) ([]interface{}, error) {
	ts.players[player.UID] = player
	return []interface{}{"hello,welcome to enter this room."}, nil
}
