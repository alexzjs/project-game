package logic

import (
	"fmt"
	Actor "gitee.com/alexzjs/project-game/core/actor"
	"gitee.com/alexzjs/project-game/core/ecs"
	"sync"
)

var Service_RoomMgr_NewRoom = "NewRoom"

type RoomManagerComponent struct {
	ecs.ComponentBase
	Actor.ActorBase
	locker     sync.RWMutex
	rooms      map[int]*RoomComponent
	increasing int //实际运用不这样,此处便宜行事
}

func (ts *RoomManagerComponent) Initialize() error {
	//初始化actor
	ts.ActorInit(ts.Parent())
	return nil
}

func (ts *RoomManagerComponent) Awake(ctx *ecs.Context) {
	//初始化房间
	ts.rooms = make(map[int]*RoomComponent)
	//注册actor消息
	ts.AddHandler(Service_RoomMgr_NewRoom, ts.NewRoom, true)
}

func (ts *RoomManagerComponent) NewRoom(message *Actor.ActorMessageInfo) error {
	r := &RoomComponent{}
	_, err := ts.Parent().AddNewbjectWithComponents([]ecs.IComponent{r})
	if err != nil {
		return err
	}

	fmt.Println("----------------------------------->aaaaaaaaaaaa----------------------------------->")

	ts.locker.Lock()
	ts.increasing++
	r.RoomID = ts.increasing
	ts.rooms[r.RoomID] = r
	ts.locker.Unlock()

	return message.Reply(r.RoomID)
}
