package gate

import (
	"errors"
	"fmt"
	"gitee.com/alexzjs/project-game/core/cluster"
	ecs2 "gitee.com/alexzjs/project-game/core/ecs"
	"gitee.com/alexzjs/project-game/utils/config"
	"gitee.com/alexzjs/project-game/utils/logger"
	network2 "gitee.com/alexzjs/project-game/utils/network"
	"reflect"
	"sync"
	"time"
)

type WSGateComponent struct {
	ecs2.ComponentBase
	locker        sync.RWMutex
	nodeComponent *Cluster.NodeComponent
	clients       sync.Map // [sessionID,*session]
	NetAPI        network2.NetAPI
	server        *network2.Server
}

func (ts *WSGateComponent) IsUnique() int {
	return ecs2.UNIQUE_TYPE_GLOBAL
}

func (ts *WSGateComponent) GetRequire() map[*ecs2.Object][]reflect.Type {
	requires := make(map[*ecs2.Object][]reflect.Type)
	requires[ts.Parent().Root()] = []reflect.Type{
		reflect.TypeOf(&config.Component{}),
	}
	return requires
}

func (ts *WSGateComponent) Awake(ctx *ecs2.Context) {
	err := ts.Parent().Root().Find(&ts.nodeComponent)
	if err != nil {
		panic(err)
	}
	if ts.NetAPI == nil {
		panic(errors.New("NetAPI is necessity of WSGateComponent"))
	}

	ts.NetAPI.Init(ts.Parent())

	conf := &network2.ServerConf{
		Proto:                "ws",
		PackageProtocol:      &network2.TdProtocol{},
		Address:              config.Config.ClusterConfig.NetListenAddress,
		ReadTimeout:          time.Millisecond * time.Duration(config.Config.ClusterConfig.NetConnTimeout),
		OnClientDisconnected: ts.OnDropped,
		OnClientConnected:    ts.OnConnected,
		NetAPI:               ts.NetAPI,
		MaxInvoke:            20,
	}

	ts.server = network2.NewServer(conf)
	err = ts.server.Serve()
	if err != nil {
		panic(err)
	}
}

func (ts *WSGateComponent) AddNetAPI(api network2.NetAPI) {
	ts.NetAPI = api
}

func (ts *WSGateComponent) OnConnected(sess *network2.Session) {
	ts.clients.Store(sess.ID, sess)
	logger.Debug(fmt.Sprintf("client [ %s ] connected,session id :[ %s ]", sess.RemoteAddr(), sess.ID))
}

func (ts *WSGateComponent) OnDropped(sess *network2.Session) {
	ts.clients.Delete(sess.ID)
	sess.PostProcessing()
}

func (ts *WSGateComponent) Destroy() error {
	ts.server.Shutdown()
	return nil
}

func (ts *WSGateComponent) SendMessage(sid string, message interface{}) error {
	if s, ok := ts.clients.Load(sid); ok {
		ts.NetAPI.Reply(s.(*network2.Session), message)
	}
	return errors.New(fmt.Sprintf("this session id: [ %s ] not exist", sid))
}

func (ts *WSGateComponent) Emit(sess *network2.Session, message interface{}) {
	ts.NetAPI.Reply(sess, message)
}

func (ts *WSGateComponent) Broadcast(message interface{}) {
	ts.clients.Range(func(key, value interface{}) bool {
		ts.NetAPI.Reply(value.(*network2.Session), message)
		return true
	})
}
